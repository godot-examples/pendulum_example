extends RigidBody


func _ready():
	# Give each pendulum an initial push.
	# The pendulum with linear_damping and angular_damping set to 0.0 will keep
	# swinging indefinitely. The pendulum with positive damping values will
	# eventually slow down and stop.
	apply_impulse(Vector3.ZERO, Vector3.LEFT * 10.0)
